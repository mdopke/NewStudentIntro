from PyMD.functions import add_water, minimize
from PyMD.System import Box, Atom
import numpy as np
from PyMD.IO import write_lammpsdata

# do separately equilibration

#time counter
import time
start_time = time.time()

# define box
# box always has to be at least twice the cut off
box = Box()
box.maxs = np.ones(3)*16 # 12.5 # 16 #10(or)
box.mins = -box.maxs
box.lengths = box.maxs - box.mins

# make water in box
system = add_water(0.5, box, typeO=1, typeH=2)
# assign box to system
system.box = box
# write dictionary for convenience
system.type_dict = {1: 'O', 2: 'H'}

# define ion
Na = Atom('Na', type=3, mass=22.994, charge=+1, comment='My first ion')
Cl = Atom('Cl', type=4, mass=35.45, charge=-1, comment='My second ion')

# find insert position
xyz0 = (np.random.rand(3)-0.5) * system.box.lengths
xyz = minimize(system, xyz0, maxIter=100, region='local')

# insert
# system.insert_atom(atom=Na, pos=xyz)
system.wrap()

write_lammpsdata('/home/tdk/git/MD_playground/in.lammpsdata', system)


from PyMD.force_fields import read_ff, write_forcefield


# define dictionary of what should be in the force field
# atoms = {1: 'Ow', 2: 'Hw', 3:'Na', 4:'Cl'}
atoms = {1: 'Ow', 2: 'Hw'}
bonds = {1:'OwHw'}
angles = {1:'HwOwHw'}

# read force fields
# FF = read_ff('/home/tdk/git/PyMD/force_fields/Zeron2019_Vega', bonded=True) # TIP4P2005 + ions
FF = read_ff('/home/tdk/git/PyMD/force_fields/water/TIP4P2005', bonded=True) # TIP4P2005

# change for example Na-Ow interaction of sigma
# FF.s[FF.s_header['Na'], FF.s_header['Ow']] = 2.0

# merge force fields
# FF = merge_ffs(FF_1, FF_2)

write_forcefield('/home/tdk/git/MD_playground/in.forcefield',
                 'lj/cut/tip4p/long 1 2 1 1 0.1546 10.0 10.0', # functional
                 atoms, bonds, angles, FF) # input data


print("--- %s seconds ---" % (time.time() - start_time))


