
Author:
Max Dopke
m.f.dopke@tudelft.nl

# ------------------------------------------------------------------------ #
# create a LAMMPS data file containing water molecules
python <directory>/PyMD/TOOLS/write_LAMMPSDATA_water.py --fout <directory>/NewStudentIntro/in.lammpsdata --dens 1 --box 20 20 20

# add ions to the data file
python <directory>/PyMD/TOOLS/write_LAMMPSDATA_ions.py --fin <directory>/NewStudentIntro/in.lammpsdata --fout <directory>/NewStudentIntro/in.lammpsdata --box 20 20 20 --ion symb:Na, type:3, add:10 --ion symb:Cl, add:10, type:4

# ------------------------------------------------------------------------ #
install VMD (on your computer)

# ------------------------------------------------------------------------ #
# install LAMMPS 16 March 2018
On Reynolds I will do this for you, on your computer you are responsible for this

# ------------------------------------------------------------------------ #
# run LAMMPS
<path>/lmp_mpi < in.simulation

# submit on cluster with submit file
sbatch submit

# ------------------------------------------------------------------------ #
TASKS TO FAMILIARIZE YOURSELF

1. Run your first simulation and visualize it in VMD
2. Change the force field in in.forcefield to SPC/E water and JC ions

    http://www1.lsbu.ac.uk/water/water_models.html
    https://aip.scitation.org/doi/full/10.1063/1.2121687
    https://pubs.acs.org/doi/abs/10.1021/jp8001614

3. Change the simulation from NVT to NPT with 1 bar pressure
4. Do a benchmark
    
    Run a simulation on:
        1 core, 2 cores, 4 cores, 8 cores, 16 cores and 28 cores
    with water box sizes of
        L = 20 20 20
        L = 40 40 40
        L = 80 80 80
    
    Equilibrate every system and write the output after the initialization using 
    
        write_data out.lammpsdata nocoeff

    Start the simulations for the benchmark by using out.lammpsdata as input instead of in.lammpsdata and removing the minimization and initialization
    Additionally, initialize the velocities using the velocity command from LAMMPS (use google)

    Run each benchmark simulation for 10000 steps at a time step of 1 fs

5. Consul your supervisor on next steps

# ------------------------------------------------------------------------ #
PLOTTING
The esiest way of plotting log files is with GNUPLOT

gnuplot
plot "path to file" u col1:col2 w l

# ------------------------------------------------------------------------ #
in main.py as some python routines as you can use them to create your files
